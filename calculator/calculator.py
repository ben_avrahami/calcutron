import asyncio
import json
from builtins import ConnectionError
from typing import Any, Dict

from aio_pika import connect, Queue
from aio_pika.pool import Pool
from aioredis import Redis, create_redis_pool

import operator as op


def pow_(n0, n1):
    if n0 == n1 == 0:
        raise ValueError('0^0 is undefined')
    return pow(n0, n1)


ops = {
    "mul": op.mul,
    "add": op.add,
    "sub": op.sub,
    "div": op.truediv,
    "pow": pow_
}


def calculate(op: str, n0: float, n1: float):
    try:
        return {'result': ops[op](n0, n1), 'status': 'ok'}
    except (ValueError, ArithmeticError):
        return {'result': None, 'status': 'error'}


async def connect_to_redis() -> Redis:
    return await create_redis_pool('redis://redis')


async def connect_to_rabbit() -> Any:
    @Pool
    async def connection_pool():
        return await connect('amqp://guest:guest@rabbit/')

    return connection_pool


async def establish_channel_pool() -> Pool:
    connection_pool = await connect_to_rabbit()

    @Pool
    async def channel_pool():
        async with connection_pool.acquire() as connection:
            return await connection.channel()

    while True:
        try:
            async with channel_pool.acquire():
                pass
            break
        except ConnectionError:  # pragma: no cover
            print('rabbit unavailable, retrying')
            await asyncio.sleep(2)

    return channel_pool


async def main():
    redis_conn = await connect_to_redis()
    channel_pool = await establish_channel_pool()

    print('ready to receive')

    async with channel_pool.acquire() as channel:
        queue: Queue = await channel.declare_queue('calc-q')
        async with queue.iterator() as messages:
            async for message in messages:
                body: Dict[str, Any] = json.loads(str(message.body, "ascii"))
                id_ = body.pop('id')
                result: Dict[str, Any] = calculate(**body)
                await redis_conn.set(id_, bytes(json.dumps(result), "ascii"))
                await message.ack()


if __name__ == '__main__':  # pragma: no cover
    asyncio.run(main())
