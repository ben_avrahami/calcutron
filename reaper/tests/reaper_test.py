import asyncio
import json
from unittest.mock import call

import pytest

import reaper
from reaper import main

try:
    from common.mocks import *
except ImportError:
    import sys

    sys.path.append('..')
    from common.mocks import *


@pytest.fixture(autouse=True)
def mock_redis(monkeypatch):
    inner_dict = {'0': b'\x00', '2': b'\x02'}
    return MockRedis(inner_dict).patch_into(reaper, monkeypatch)


@pytest.fixture(autouse=True)
def mock_rabbit(monkeypatch):
    return MockRabbit().patch_into(reaper, monkeypatch)


@pytest.fixture(autouse=True)
def mock_kafka(monkeypatch):
    return MockKafkaProducer().patch_into(reaper, monkeypatch)


def test_consume(mock_rabbit, mock_redis, mock_kafka):
    def queue_reap(uuid):
        mock_rabbit.enqueue(
            'reaper-q',
            bytes(json.dumps({
                "id": uuid,
            }), "ascii")
        )

    queue_reap('0')
    queue_reap('1')
    queue_reap('2')

    asyncio.run(main())

    mock_redis.get.assert_has_calls([
        call('0'),
        call('1'),
        call('2'),
    ])

    mock_redis.delete.assert_has_calls([
        call('0'),
        call('2'),
    ])

    mock_kafka.send.assert_has_calls([
        call('reaped-calculations', b'\x00'),
        call('reaped-calculations', b'\x02')
    ])
