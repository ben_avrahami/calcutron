from __future__ import annotations

from collections import defaultdict
from functools import cached_property
from typing import ClassVar, Type
from unittest.mock import AsyncMock, Mock, MagicMock


class PatchIn:
    patch_key: ClassVar[str]
    patch_factory: ClassVar[Type[Mock]] = AsyncMock

    def patch_into(self, module, monkeypatch):
        monkeypatch.setattr(module, self.patch_key, self.patch_factory(return_value=self))
        return self

    def _into_mock(self, func, factory):
        mock = factory(side_effect=func)
        setattr(self, func.__name__, mock)
        return mock

    def into_mock(self, func):
        return self._into_mock(func, MagicMock)

    def into_mock_async(self, func):
        return self._into_mock(func, AsyncMock)


class MockMessageIterator:
    def __init__(self, owner: MockRabbit, routing_key: str):
        self.owner = owner
        self.routing_key = routing_key

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        return exc_type

    async def __aiter__(self):
        for em in self.owner.enqueued[self.routing_key]:
            yield MagicMock(body=em, ack=AsyncMock())


class MockQueue:
    def __init__(self, owner: MockRabbit, routing_key: str):
        self.owner = owner
        self.routing_key = routing_key

    async def bind(self, *args, **kwargs):
        pass

    def iterator(self):
        return MockMessageIterator(self.owner, self.routing_key)


class MockExchange:
    def __init__(self, owner: MockRabbit):
        self.owner = owner

    async def publish(self, msg, routing_key: str):
        self.owner.enqueue(routing_key, msg.body)


class MockChannel:
    def __init__(self, owner: MockRabbit):
        self.owner = owner

    async def declare_queue(self, name: str, *args, **kwargs):
        return MockQueue(self.owner, name)

    @cached_property
    def default_exchange(self):
        return MockExchange(self.owner)


class MockRabbit(PatchIn):
    patch_key = 'connect'

    def __init__(self):
        self.enqueued = defaultdict(list)

    def enqueue(self, key, data):
        self.enqueued[key].append(data)

    async def channel(self):
        return MockChannel(self)


class MockRedis(PatchIn):
    patch_key = 'create_redis_pool'

    def __init__(self, starter_dict=None):
        self.inner = starter_dict or {}

        @self.into_mock_async
        def get(k, default=None):
            return self.inner.get(k, default)

        @self.into_mock_async
        def set(k, v):
            self.inner[k] = v

        @self.into_mock_async
        def exists(k):
            return k in self.inner

        @self.into_mock_async
        def delete(k):
            try:
                del self.inner[k]
            except KeyError:
                return 0
            return 1


class MockKafkaProducer(PatchIn):
    patch_key = 'AIOKafkaProducer'
    patch_factory = Mock

    def __init__(self):
        @self.into_mock_async
        def start():
            pass

        @self.into_mock_async
        def send(key, value):
            pass


__all__ = ['MockRabbit', 'MockRedis', 'MockKafkaProducer']
