pip install poetry
poetry install --no-root
poetry run python -m pytest
poetry run coverage run --source=. --omit="*_test.py" -m pytest .
poetry run coverage report --fail-under=100 -m