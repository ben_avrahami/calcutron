import json
from subprocess import run, PIPE, DEVNULL
from time import sleep

import requests
from requests import HTTPError, ConnectionError, ReadTimeout


def e2e():
    run('docker-compose up --build -d', stdout=DEVNULL)

    try:
        query_ttl = float(
            run('docker-compose exec -T app printenv QUERY_TTL', stdout=PIPE, text=True).stdout.split('\n', 2)[0]
        )
        host_port = int(
            run('docker-compose port app 8000', stdout=PIPE, text=True).stdout.rsplit(':', 2)[-1].split('\n', 2)[0]
        )

        while True:
            try:
                r = requests.get(f"http://localhost:{host_port}/docs", timeout=3)
                r.raise_for_status()
            except (ConnectionError, HTTPError, ReadTimeout) as e:
                print(f'API connection failed ({e!r}) retrying...')
                sleep(2)
                break

        r0 = requests.get("http://localhost/query/add/5/6")
        r0.raise_for_status()
        b0 = json.loads(r0.text)
        id_ = b0.pop('id')
        assert not b0
        print('t0')

        while True:
            r1 = requests.get(f"http://localhost/result/{id_}")
            r1.raise_for_status()
            b1 = json.loads(r1.text)
            if b1['status'] != 'pending_or_vacant':
                break

        assert b1 == {'status': 'ok', 'result': 11.0}
        print('t1')

        sleep(query_ttl)

        r2 = requests.get(f"http://localhost/result/{id_}")
        r2.raise_for_status()
        b2 = json.loads(r2.text)
        assert b2 == {'status': 'pending_or_vacant', 'result': None}
        print('t2')

        output = run('docker-compose logs printer', stdout=PIPE, text=True).stdout
        assert output.count("expunged result: {'result': 11.0, 'status': 'ok'}") == 1
        print('t3')
    finally:
        run('docker-compose down')


if __name__ == '__main__':
    e2e()
