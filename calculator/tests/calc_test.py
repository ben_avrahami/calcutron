import asyncio
import json
from unittest.mock import call

import pytest

import calculator
from calculator import calculate, main

try:
    from common.mocks import *
except ImportError:
    import sys

    sys.path.append('..')
    from common.mocks import *


def test_ops():
    assert calculate("add", 1.0, 3.0) == {
        'result': 4.0,
        'status': 'ok'
    }
    assert calculate("div", 1.0, 2.0) == {
        'result': 0.5,
        'status': 'ok'
    }
    assert calculate("pow", 1.0, 3.0) == {
        'result': 1.0,
        'status': 'ok'
    }
    assert calculate("pow", 0.0, 0.0) == {
        'result': None,
        'status': 'error'
    }


@pytest.fixture(autouse=True)
def mock_redis(monkeypatch):
    return MockRedis().patch_into(calculator, monkeypatch)


@pytest.fixture(autouse=True)
def mock_rabbit(monkeypatch):
    return MockRabbit().patch_into(calculator, monkeypatch)


def test_consume(mock_rabbit, mock_redis):
    def queue_calc(uuid, op, a, b):
        mock_rabbit.enqueue(
            'calc-q',
            bytes(json.dumps({
                "id": uuid,
                "op": op,
                "n0": a,
                "n1": b
            }), "ascii")
        )

    queue_calc('0', 'add', 1.25, 3.65)
    queue_calc('1', 'mul', 3.0, 2.0)
    queue_calc('2', 'div', 5.0, 0.0)

    asyncio.run(main())

    calls = [
        call('0', bytes(json.dumps({
            'result': 4.9,
            'status': 'ok'
        }), "ascii")),
        call('1', bytes(json.dumps({
            'result': 6.0,
            'status': 'ok'
        }), "ascii")),
        call('2', bytes(json.dumps({
            'result': None,
            'status': 'error'
        }), "ascii")),
    ]

    mock_redis.set.assert_has_calls(calls)
