import json

import faust

app = faust.App('printer', broker=r"kafka://broker:9092", store=r"memory://", consumer_auto_offset_reset='latest')

topic = app.topic('reaped-calculations', value_type=bytes, value_serializer='raw')


@app.agent(topic)
async def consume(stream):
    async for msg in stream:
        body = json.loads(str(msg, 'ascii'))
        app.output(str(body))
        yield body

if __name__ == '__main__':  # pragma: no cover
    print('starting')
    app.output = lambda s: print(f"expunged result: {s}")
    app.main()
