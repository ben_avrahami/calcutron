import asyncio
import json
from typing import Dict, Any

from aio_pika.pool import Pool
from aiokafka import AIOKafkaProducer
from aioredis import Redis, create_redis_pool
from aio_pika import connect, Queue

from kafka.errors import KafkaConnectionError


async def connect_to_redis() -> Redis:
    return await create_redis_pool('redis://redis')


async def connect_to_rabbit() -> Pool:
    async def get_connection():
        return await connect('amqp://guest:guest@rabbit/')

    return Pool(get_connection)


async def establish_channel_pool() -> Pool:
    connection_pool = await connect_to_rabbit()

    async def get_channel():
        async with connection_pool.acquire() as connection:
            return await connection.channel()

    channel_pool = Pool(get_channel)

    while True:
        try:
            async with channel_pool.acquire():
                pass
            break
        except ConnectionError:  # pragma: no cover
            print('rabbit unavailable, retrying')
            await asyncio.sleep(2)

    return channel_pool


async def connect_to_kafka():
    producer = AIOKafkaProducer(bootstrap_servers='broker:9092')
    while True:
        try:
            await producer.start()
            break
        except KafkaConnectionError:  # pragma: no cover
            print(f'kafka unavailable, retrying')
            await asyncio.sleep(2)
    return producer


async def main():
    redis_conn = await connect_to_redis()
    kafka_conn = await connect_to_kafka()
    channel_pool = await establish_channel_pool()

    print('ready to receive')

    async with channel_pool.acquire() as channel:
        queue: Queue = await channel.declare_queue('reaper-q')
        async with queue.iterator() as messages:
            async for message in messages:
                body: Dict[str, Any] = json.loads(str(message.body, "ascii"))
                id_ = body.pop('id')
                value = await redis_conn.get(id_)
                if value is None:
                    print(f'failed to delete {id_}')
                else:
                    await redis_conn.delete(id_)
                    await kafka_conn.send("reaped-calculations", value)
                    print(f'deleted {id_}')


if __name__ == '__main__':  # pragma: no cover
    asyncio.run(main())
