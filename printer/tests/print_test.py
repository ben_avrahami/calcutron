import asyncio
import json
from unittest.mock import Mock

from printer import app, consume


def test_consume():
    async def _test_consume():
        app.output = Mock()
        async with consume.test_context() as agent:
            await agent.put(bytes(json.dumps({
                'result': 3.5, 'status': 'ok'
            }), 'ascii'))
        app.output.assert_called_once_with("{'result': 3.5, 'status': 'ok'}")

    asyncio.run(_test_consume())
