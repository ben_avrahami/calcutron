import json
from unittest.mock import call

import pytest
from starlette.testclient import TestClient

import app as app_module
from app import app, QueryOutput, ResultOutput, Status, redis_conn

try:
    from common.mocks import *
except ImportError:
    import sys

    sys.path.append('..')
    from common.mocks import *


@pytest.fixture(autouse=True)
def mock_redis(monkeypatch):
    inner_dict = {'extant': bytes(json.dumps({'result': 3.5, 'status': 'ok'}), 'ascii')}
    return MockRedis(inner_dict).patch_into(app_module, monkeypatch)


@pytest.fixture(autouse=True)
def mock_rabbit(monkeypatch):
    return MockRabbit().patch_into(app_module, monkeypatch)


def test_get_id(mock_rabbit: MockRabbit):
    with TestClient(app) as client:
        response = client.get("/query/add?a=1&b=2")
        assert response.status_code == 200
        id_ = response.json()['id']
        assert response.json() == QueryOutput(id=id_)
        assert mock_rabbit.enqueued['calc-q'] == [
            bytes(json.dumps(
                {
                    "id": id_,
                    "op": 'add',
                    "n0": 1.0,
                    "n1": 2.0
                }), "ascii")
        ]
        assert mock_rabbit.enqueued['reaper-wr'] == [
            bytes(json.dumps(
                {
                    "id": id_
                }), "ascii")
        ]


def test_pending():
    with TestClient(app) as client:
        id_ = client.get("/query/add?a=1&b=2").json()['id']
        response = client.get(f"/result/{id_}")
        assert response.status_code == 200
        assert response.json() == ResultOutput(status=Status.pending, result=None)
        redis_conn().get.assert_has_calls([
            call(id_)
        ])


def test_extant():
    with TestClient(app) as client:
        response = client.get(f"/result/extant")
        assert response.status_code == 200
        assert response.json() == ResultOutput(status=Status.complete, result=3.5)
        redis_conn().get.assert_has_calls([
            call('extant')
        ])
