import json
import os
from enum import Enum
from typing import Any, Optional, cast
from uuid import uuid1

from aio_pika import Message, connect, Channel
from aio_pika.pool import Pool, PoolItemContextManager
from aioredis import Redis, create_redis_pool
from fastapi import FastAPI
from pydantic.main import BaseModel

app = FastAPI()


def redis_conn() -> Redis:
    return cast(Redis, app.extra['redis_conn'])


def rabbit_channel() -> PoolItemContextManager[Channel]:
    return cast(Pool, app.extra['rabbit_channel_pool']).acquire()


async def connect_to_redis() -> Any:  # hack to cover FastAPI's bad typing
    return await create_redis_pool('redis://redis')


async def connect_to_rabbit() -> Any:
    async def get_connection():
        return await connect('amqp://guest:guest@rabbit/')

    return Pool(get_connection)


async def establish_channel_pool(connection_pool: Pool, waiting_room_delay) -> Any:
    async def get_channel():
        async with connection_pool.acquire() as connection:
            return await connection.channel()

    channel_pool = Pool(get_channel)

    async with channel_pool.acquire() as channel:
        await channel.declare_queue('calc-q')

        reaper_q = await channel.declare_queue('reaper-q')
        await reaper_q.bind('amq.direct')

        # we declare the waiting room as the queue messages will be sent to, with a ttl ans fallback right into the real
        # reaper queue
        await channel.declare_queue("reaper-wr", durable=True, arguments={
            'x-message-ttl': int(waiting_room_delay * 1000),
            'x-dead-letter-exchange': 'amq.direct',
            'x-dead-letter-routing-key': 'reaper-q'
        })

    return channel_pool


@app.on_event("startup")
async def startup_event():
    app.extra['redis_conn'] = await connect_to_redis()
    rabbit_connection_pool = await connect_to_rabbit()
    delay = float(os.environ.get('QUERY_TTL', 10))
    app.extra['rabbit_channel_pool'] = await establish_channel_pool(rabbit_connection_pool, delay)


class Operator(str, Enum):
    add = "add"
    sum = "sub"
    mul = "mul"
    div = "div"
    pow = "pow"


async def gen_vacant_uuid() -> str:
    return str(uuid1())


class QueryOutput(BaseModel):
    id: str


@app.get("/query/{op}", response_model=QueryOutput)
async def query(op: str, a: float, b: float) -> QueryOutput:
    uuid = await gen_vacant_uuid()
    async with rabbit_channel() as channel:
        calc_body = \
            bytes(json.dumps({
                "id": uuid,
                "op": op,
                "n0": a,
                "n1": b
            }), "ascii")
        reap_body = \
            bytes(json.dumps({
                "id": uuid
            }), "ascii")
        await channel.default_exchange.publish(Message(calc_body), routing_key="calc-q")
        await channel.default_exchange.publish(Message(reap_body), routing_key='reaper-wr')

    return QueryOutput(id=uuid)


class Status(str, Enum):
    # note that we can't actually tell whether a query is not yet calculated or already deleted
    # todo instead of checking whether the key exists in the redis base upon creation, we set it by default to
    #  "pending" or something
    pending = "pending_or_vacant"
    complete = "ok"
    error = "error"


class ResultOutput(BaseModel):
    status: Status
    result: Optional[float] = None


@app.get("/result/{id}", response_model=ResultOutput)
async def result(id: str) -> ResultOutput:
    rc = redis_conn()
    res = await rc.get(id)
    if res is None:
        return ResultOutput(status=Status.pending)
    res = json.loads(res)
    return ResultOutput(**res)


if __name__ == '__main__':  # pragma: no cover
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
